// Copyright Epic Games, Inc. All Rights Reserved.

#include "CPPTestProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, CPPTestProject, "CPPTestProject" );
