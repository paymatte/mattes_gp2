// Fill out your copyright notice in the Description page of Project Settings.


#include "Week1/ExampleCPPActor.h"

// Sets default values
AExampleCPPActor::AExampleCPPActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

#pragma region Unreal Functions
// Called when the game starts or when spawned
void AExampleCPPActor::BeginPlay()
{
	Super::BeginPlay();
	
	//Update the actor's location when the Game starts
	//FVector NewLocation = FVector(500.f, 500.f, 200.f);
	//SetActorLocation(NewLocation);
}

// Called every frame
void AExampleCPPActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//if (GetActorLocation().X < 1000.f) {
		//FVector Offset = FVector(MoveSpeed * DeltaTime, 0.f, 0.f);
		//AddActorLocalOffset(Offset);
	//}


}
/*
void AExampleCPPActor::DoThing() 
{
	UE_Log(LogTemp, Display, TEXT("Do a thing"));
}

int32 AExampleCPPActor::ReturnThing(int32 A, int32 B)
{
	return A+B;
}
#pragma endregion

*/