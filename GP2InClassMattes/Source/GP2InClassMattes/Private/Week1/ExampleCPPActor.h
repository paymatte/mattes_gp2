// Fill out your copyright notice in the Description page of Project Settings.

#pragma once //makes sure that a class can only be included once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ExampleCPPActor.generated.h"

UCLASS()//unreal macro (all classes need this macro)
class AExampleCPPActor : public AActor
{
	GENERATED_BODY()//sets up for us must be first 
	
public:	
	// Sets default values for this actor's properties
	AExampleCPPActor();

#pragma region Unreal Functions

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

#pragma endregion

public://varibales
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Examples")//see it on the blueprint or prefab
		int32 VisibleAnywhereInt = 12;

	//Defalust = blueprints
	//Editor = Instance

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Examples")//
		int32 EditAnywhereInt = 12;

	UPROPERTY(VisibleInstanceOnly, Category = "Examples")//see on the 
		int32 VisibleInstanceOnlyInt = 12;

	UPROPERTY(VisibleDefaultsOnly, Category = "Examples")//see the value on the blueprint
		int32 VisibleDefaultsOnlyInt = 12;

	UPROPERTY(EditDefaultsOnly, Category = "Examples")//see the value on the blueprint
		int32 EditDefaultsOnlyInt = 12;

public://functions
	/*UFUNCTIONS(BlueprintCallable)
	void DoThing();
	UFUNCTIONS(BlueprintPure)
	int32 ReturnThing(int32 A, int32 B);*/
protected:

protected:

private:
	UPROPERTY(EditAnywhere, Category = "Movement")
	float MoveSpeed = 100.f;
	
private:

};
