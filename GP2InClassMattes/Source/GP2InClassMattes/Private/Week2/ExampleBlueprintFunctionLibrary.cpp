// Fill out your copyright notice in the Description page of Project Settings.


#include "Week2/ExampleBlueprintFunctionLibrary.h"

FVector UExampleBlueprintFunctionLibrary::GetRandomUnitVector() {
	FVector RandomDirection = FVector(FMath::RandRange(-1.f, 1.f), FMath::RandRange(-1.f, 1.f), FMath::RandRange(-1.f, 1.f));

	RandomDirection = RandomDirection.GetSafeNormal();
	return RandomDirection;
}