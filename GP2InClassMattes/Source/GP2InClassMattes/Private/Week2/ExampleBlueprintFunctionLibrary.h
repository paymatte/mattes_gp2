// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "ExampleBlueprintFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class UExampleBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public: 
	UFUNCTION(BlueprintCallable, Category = "Example Functions")
	static FVector GetRandomUnitVector();
	
};
