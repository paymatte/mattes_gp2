// Fill out your copyright notice in the Description page of Project Settings.


#include "Week2/RandomMovementComponent.h"
#include "Week2/ExampleBlueprintFunctionLibrary.h"

// Sets default values for this component's properties
URandomMovementComponent::URandomMovementComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void URandomMovementComponent::BeginPlay()
{
	Super::BeginPlay();
	
	
	//get random direction and normalize it
	/*
	RandomDirection = FVector(FMath::RandRange(-1.f,1.f), FMath::RandRange(-1.f, 1.f), FMath::RandRange(-1.f, 1.f));
	RandomDirection = RandomDirection.GetSafeNormal();*/

	RandomDirection = UExampleBlueprintFunctionLibrary::GetRandomUnitVector();
	
}


// Called every frame
void URandomMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//get the actor that owns this compoenet
	AActor* OwningActor = GetOwner();
	FVector CurrentLoc = OwningActor->GetActorLocation();

	//calculate a new loaction
	FVector NewLocation = CurrentLoc + (RandomDirection * MoveSpeed)*DeltaTime;
	OwningActor->SetActorLocation(NewLocation);

}




