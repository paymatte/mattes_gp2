// Fill out your copyright notice in the Description page of Project Settings.


#include "Week3/CDOExample.h"
#include "Week2/RandomMovementComponent.h"

// Sets default values
ACDOExample::ACDOExample()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("My Static Mesh"));
	SetRootComponent(StaticMesh);//can only do with scene component
	//StaticMesh->SetupAttachment(GetRootComponent);//attach this to root component

	RandomMovement = CreateDefaultSubobject<URandomMovementComponent>(TEXT("My Random Movement"));
}

// Called when the game starts or when spawned
void ACDOExample::BeginPlay()
{
	Super::BeginPlay();

	FVector SpawnLoc = FVector(200.f, 200.f, 100.f);

	AActor* SpawnedActor = GetWorld()->SpawnActor<AActor>(ActorClass, SpawnLoc, FRotator::ZeroRotator);
	
}

// Called every frame
void ACDOExample::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

