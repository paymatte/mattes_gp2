// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
//#include "Week2/RandomMovementComponent.h"
#include "CDOExample.generated.h"

//forward declaration
class URandomMovementComponent;

UCLASS()
class ACDOExample : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACDOExample();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private: 
	UPROPERTY(EditAnywhere, Category = "Components")
	TObjectPtr<UStaticMeshComponent> StaticMesh;	

	UPROPERTY(EditAnywhere, Category = "Components")
	TObjectPtr<URandomMovementComponent> RandomMovement;

	UPROPERTY(EditAnywhere, Category = "Actor Spawning")
	TObjectPtr<UClass> AnyClass;
	
	UPROPERTY(EditAnywhere, Category = "Actor Spawning")
	TSubclassOf<AActor> ActorClass;


};
