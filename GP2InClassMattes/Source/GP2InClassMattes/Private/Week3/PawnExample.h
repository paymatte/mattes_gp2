// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PawnExample.generated.h"

class UInputMappingContext;
class UInputAction;
struct FInputActionValue;
class USpringArmComponent;
class UCameraComponent;

UCLASS()
class APawnExample : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APawnExample();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	UPROPERTY(EditAnywhere, Category = "Components")
	TObjectPtr<UStaticMeshComponent> StaticMesh;
	UPROPERTY(EditAnywhere, Category = "Components")
	TObjectPtr<UStaticMeshComponent> TurretMesh;

	UPROPERTY(EditAnywhere, Category = "Input")
	TObjectPtr<UInputMappingContext> PawnContext;

	UPROPERTY(EditAnywhere, Category = "Input")
	TObjectPtr<UInputAction> MoveAction;

	UPROPERTY(EditAnywhere, Category = "Input")
	TObjectPtr<UInputAction> LookAction;

	//UPROPERTY(EditAnywhere, Category = "Input")
	//TObjectPtr<UInputAction> ShootAction;

	UPROPERTY(EditAnywhere, Category = "Component")
	TObjectPtr<USpringArmComponent> SpringArmComponent;
	UPROPERTY(EditAnywhere, Category = "Component")
	TObjectPtr<UCameraComponent> CameraComponent;



private:
	UPROPERTY(EditAnywhere, Category = "Movement")
	float MoveSpeed = 400.f;	
	UPROPERTY(EditAnywhere, Category = "Movement")
	float RotationSpeed = 180.f;

private:
	void MoveCallback(const FInputActionValue& Value);
	void LookCallback(const FInputActionValue& Value);
	//void ShootCallback(const FInputActionValue& Value);
	void RotateTurret();

};
