// Fill out your copyright notice in the Description page of Project Settings.


#include "Week3/PlayerControllerExample.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"


void APlayerControllerExample::BeginPlay()
{
	Super::BeginPlay();


	if (UEnhancedInputLocalPlayerSubsystem* EILPSubsystem =
		ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer())) {
		if (EILPSubsystem) {
			EILPSubsystem->AddMappingContext(ControllerContext, 0);
		}

	}
}

void APlayerControllerExample::SetupInputComponent()
{
	Super::SetupInputComponent();
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent)) {
		EnhancedInputComponent->BindAction(PauseAction, ETriggerEvent::Triggered, this, &APlayerControllerExample::PauseCallback);
	}

}

void APlayerControllerExample::PauseCallback(const FInputActionValue& Value)
{
	UE_LOG(LogTemp, Display, TEXT("PauseGAME"));

	if (IsPaused()) {
		SetPause(false);
	}
	else {
		Pause();
	}
}
