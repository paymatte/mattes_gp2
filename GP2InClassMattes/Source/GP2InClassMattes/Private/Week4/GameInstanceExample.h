// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "GameInstanceExample.generated.h"

/**
 * 
 */
UCLASS()
class UGameInstanceExample : public UGameInstance
{
	GENERATED_BODY()

	
public:
	UPROPERTY(EditAnywhere, Category = "GameRules")
	int32 PersistantPlayerLives = -1;



};
