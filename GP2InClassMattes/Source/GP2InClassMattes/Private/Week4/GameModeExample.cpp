// Fill out your copyright notice in the Description page of Project Settings.


#include "Week4/GameModeExample.h"
#include "Week3/PawnExample.h"
#include "Week3/PlayerControllerExample.h"
#include "Week4/GameStateExample.h"
#include "Week4/GameInstanceExample.h"
#include "Kismet/GameplayStatics.h"

AGameModeExample::AGameModeExample()
{
	PrimaryActorTick.bCanEverTick = true;

	DefaultPawnClass = APawnExample::StaticClass();
	PlayerControllerClass = APlayerControllerExample::StaticClass();
	GameStateClass = AGameStateExample::StaticClass();
}

void AGameModeExample::BeginPlay()
{
	Super::BeginPlay();

	UGameInstanceExample* GameInstanceExample = GetWorld()->GetGameInstance<UGameInstanceExample>();

	if (GameInstanceExample->PersistantPlayerLives == -1) {
		GameInstanceExample->PersistantPlayerLives = InitialPlayerLives;
	}

	GameStateExample = GetGameState<AGameStateExample>();
	GameStateExample->TimeRemaining = GameTime;

	GameStateExample->CurrentPlayerLives = GameInstanceExample->PersistantPlayerLives;
}

void AGameModeExample::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (GameStateExample->TimeRemaining > 0.f) {
		GameStateExample->TimeRemaining -= DeltaTime;
		UE_LOG(LogTemp, Display, TEXT("TIME Remaining %f"), GameStateExample->TimeRemaining);
		if (GameStateExample->TimeRemaining <= 0.f) {
			GameStateExample->TimeRemaining = 0.f;
			UE_LOG(LogTemp, Display, TEXT("Ran out of time"));

			//Decrement number of lives
			UGameInstanceExample* GameInstanceExample = GetWorld()->GetGameInstance<UGameInstanceExample>();
			GameInstanceExample->PersistantPlayerLives--;
			if (GameInstanceExample->PersistantPlayerLives == 0) {
				GameInstanceExample->PersistantPlayerLives = -1;
				UE_LOG(LogTemp, Display, TEXT("GAMEOVER"));
			}
			else {
				UGameplayStatics::OpenLevel(this, TEXT("Week4"));
			}

		}
	}
}
