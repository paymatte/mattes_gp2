// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameModeExample.generated.h"

class AGameStateExample;

/**
 * 
 */
UCLASS()
class AGameModeExample : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGameModeExample();
protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

public:
	UPROPERTY(EditAnywhere, Category = "GameRules")
	float GameTime = 60.f;
	UPROPERTY(EditAnywhere, Category= "GameRules")
	int32 InitialPlayerLives = 3;

private:
	AGameStateExample* GameStateExample;
};
