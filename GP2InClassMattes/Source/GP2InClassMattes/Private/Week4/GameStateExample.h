// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "GameStateExample.generated.h"

/**
 * 
 */
UCLASS()
class AGameStateExample : public AGameStateBase
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, Category = "GameRules")
	float TimeRemaining = -1.f;

	UPROPERTY(EditAnywhere, Category = "GameRules")
	int32 CurrentPlayerLives = -1;
	
};
