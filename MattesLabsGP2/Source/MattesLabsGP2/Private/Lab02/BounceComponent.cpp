// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab02/BounceComponent.h"

// Sets default values for this component's properties
UBounceComponent::UBounceComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UBounceComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UBounceComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	AActor* OwningActor = GetOwner();
	FVector CurrentLoc = OwningActor->GetActorLocation();
	TimeElasped += DeltaTime;
	if (TimeElasped >= MaxTime) {
		Direction *= -1.f;
		TimeElasped = 0.f;
	}
	//calculate a new loaction
	FVector NewLocation = CurrentLoc + ( Direction * MaxSpeed) * DeltaTime;
	OwningActor->SetActorLocation(NewLocation);

	// ...
}


