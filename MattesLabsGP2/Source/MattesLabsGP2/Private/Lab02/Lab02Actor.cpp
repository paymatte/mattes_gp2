// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab02/Lab02Actor.h"

// Sets default values
ALab02Actor::ALab02Actor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ObjectMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SMesh"));
	SetRootComponent(ObjectMesh);
}

// Called when the game starts or when spawned
void ALab02Actor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALab02Actor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

