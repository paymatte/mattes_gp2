// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab02/WaypointComponent.h"
#include "LabsBPLib.h"

// Sets default values for this component's properties
UWaypointComponent::UWaypointComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWaypointComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UWaypointComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	AActor* OwningActor = GetOwner();
	FVector CurrentLoc = OwningActor->GetActorLocation();
	//calculate a new loaction
	FVector NewLocation = ULabsBPLib::MoveTowards(CurrentLoc, Waypoints[index], MoveSpeed);
	if (NewLocation == Waypoints[index]) {
		index++;
		index = index % Waypoints.Num();
	}
	OwningActor->SetActorLocation(NewLocation);
	// ...
}

