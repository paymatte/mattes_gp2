// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab03/Lab03Actor.h"
#include "Lab3SceneComponent.h"
// Sets default values
ALab03Actor::ALab03Actor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("My Static Mesh"));
	//SetRootComponent(StaticMesh);
	Lab3SceneComponent = CreateDefaultSubobject<ULab3SceneComponent>(TEXT("My Scene Component"));
	SetRootComponent(Lab3SceneComponent);



	StaticMesh->SetupAttachment(Lab3SceneComponent, "StaticMesh");
}

// Called when the game starts or when spawned
void ALab03Actor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALab03Actor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

