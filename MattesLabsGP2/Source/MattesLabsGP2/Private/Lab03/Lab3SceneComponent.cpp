// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab03/Lab3SceneComponent.h"

// Sets default values for this component's properties
ULab3SceneComponent::ULab3SceneComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void ULab3SceneComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void ULab3SceneComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	FRotator  f = GetRelativeRotation();

	//AActor* OwningActor = GetOwner();
	//FRotator  f = OwningActor->GetActorRotation();
	f.Yaw = f.Yaw + (MoveSpeed)*DeltaTime;

	SetRelativeRotation(f);

	// ...
}

