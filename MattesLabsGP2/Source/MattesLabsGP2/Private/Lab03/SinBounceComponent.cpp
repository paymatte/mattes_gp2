// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab03/SinBounceComponent.h"

// Sets default values for this component's properties
USinBounceComponent::USinBounceComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void USinBounceComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void USinBounceComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	AActor* Actor = GetOwner();

	FVector CurrentLocation = Actor->GetActorLocation();
	TimeElapsed += DeltaTime;
	FVector NewLocation = CurrentLocation;
		NewLocation.Z = CurrentLocation.Z + BounceSpeed * FMath::Sin(TimeElapsed) * DeltaTime;

		Actor->SetActorLocation(NewLocation);
	// ...
}

