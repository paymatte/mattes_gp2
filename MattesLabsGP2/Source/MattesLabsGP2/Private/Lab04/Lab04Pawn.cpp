// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab04/Lab04Pawn.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
// Sets default values
ALab04Pawn::ALab04Pawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	SetRootComponent(StaticMesh);

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));

	SpringArm->SetupAttachment(GetRootComponent());
	Camera->SetupAttachment(SpringArm);


}

// Called when the game starts or when spawned
void ALab04Pawn::BeginPlay()
{
	Super::BeginPlay();
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller)) {
		if (ULocalPlayer* LocalPlayer = PlayerController->GetLocalPlayer()) {
			if (UEnhancedInputLocalPlayerSubsystem* EILPSubsystem =
				ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(LocalPlayer)) {
				if (EILPSubsystem) {
					EILPSubsystem->AddMappingContext(PawnContext, 0);
				}

			}
		}

	}
}

// Called every frame
void ALab04Pawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ALab04Pawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent)) {
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ALab04Pawn::MoveCallback);
		EnhancedInputComponent->BindAction(SteerAction, ETriggerEvent::Triggered, this, &ALab04Pawn::SteerCallback);
	}
}

void ALab04Pawn::MoveCallback(const FInputActionValue& Value)
{
	float DeltaTime = GetWorld()->GetDeltaSeconds();

	FVector forward = FVector(1, 0, 0);

	FVector MoveDirection = forward * MoveSpeed * DeltaTime;

	AddActorLocalOffset(MoveDirection, true);
	UE_LOG(LogTemp, Display, TEXT("MOVEMOVEMOVE"));

}

void ALab04Pawn::SteerCallback(const FInputActionValue& Value)
{
	FVector2D MoveInput = Value.Get<FVector2D>();
	UE_LOG(LogTemp, Display, TEXT("MOVE: %s"), *MoveInput.ToString());
	//type UELOg
	//UE_LOG(LogTemp, Display, TEXT(""));
	float DeltaTime = GetWorld()->GetDeltaSeconds();

	//FVector MoveDirection = GetActorForwardVector() * MoveInput.Y * MoveSpeed * DeltaTime;
	//AddActorWorldOffset(MoveDirection, true);

	FVector up = FVector(0, 0, 1);
	FVector right = FVector(0, 1, 0);

	FQuat RotationAmount = FQuat(up , MoveInput.X * FMath::DegreesToRadians(RotationSpeed) * DeltaTime);

	AddActorLocalRotation(RotationAmount, true);

	FQuat RotationAmountTwo = FQuat(right, MoveInput.Y * FMath::DegreesToRadians(RotationSpeed) * DeltaTime);

	AddActorLocalRotation(RotationAmountTwo, true);


	//AddActorLocalRotation(RotationAmount, true);

	
}

