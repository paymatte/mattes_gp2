// Fill out your copyright notice in the Description page of Project Settings.


#include "LabsBPLib.h"

FVector ULabsBPLib::MoveTowards(const FVector& startPos, const FVector& endPos, float maxDistance)
{
	FVector direction = endPos - startPos;
	direction = direction.GetSafeNormal();
	FVector nextPos = (direction * maxDistance) + startPos;
	if (startPos.X < endPos.X) {
		if (nextPos.X >= endPos.X) {
			nextPos.X = endPos.X;
		}
	}
	else if (startPos.X > endPos.X) {
		if (nextPos.X <= endPos.X) {
			nextPos.X = endPos.X;
		}
	}
	else {
		nextPos.X = endPos.X;
	}
	if (startPos.Y < endPos.Y) {
		if (nextPos.Y >= endPos.Y) {
			nextPos.Y = endPos.Y;

		}
	}
	else if (startPos.Y > endPos.Y) {
		if (nextPos.Y <= endPos.Y) {
			nextPos.Y = endPos.Y;
		}
	}
	else {
		nextPos.Y = endPos.Y;
	}
	if (startPos.Z < endPos.Z) {
		if (nextPos.Z >= endPos.Z) {
			nextPos.Z = endPos.Z;
		}
	}
	else if (startPos.Z > endPos.Z) {
		if (nextPos.Z <= endPos.Z) {
			nextPos.Z = endPos.Z;
		}
	}
	else {
		nextPos.Z = endPos.Z;
	}
	return nextPos;
}
