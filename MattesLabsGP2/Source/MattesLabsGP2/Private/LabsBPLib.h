// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "LabsBPLib.generated.h"

/**
 * 
 */
UCLASS()
class ULabsBPLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "Movement")
	static FVector MoveTowards(const FVector& startPos, const FVector& endPos, float maxDistance);
};
