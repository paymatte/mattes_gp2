// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyActorComponent.h"

// Sets default values for this component's properties
UEnemyActorComponent::UEnemyActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UEnemyActorComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UEnemyActorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	AActor* OwningActor = GetOwner();
	FVector CurrentLoc = OwningActor->GetActorLocation();
	//calculate a new loaction
	FVector NewLocation = UEnemyActorComponent::MoveTowards(CurrentLoc, Waypoints[index], MoveSpeed);
	if (NewLocation == Waypoints[index]) {
		index++;
		index = index % Waypoints.Num();
	}
	OwningActor->SetActorLocation(NewLocation);
	// ...
}
FVector UEnemyActorComponent::MoveTowards(FVector& startPos, FVector& endPos, float maxDistance)
{
	FVector direction = endPos - startPos;
	direction = direction.GetSafeNormal();
	FVector nextPos = (direction * maxDistance) + startPos;
	if (startPos.X < endPos.X) {
		if (nextPos.X >= endPos.X) {
			nextPos.X = endPos.X;
		}
	}
	else if (startPos.X > endPos.X) {
		if (nextPos.X <= endPos.X) {
			nextPos.X = endPos.X;
		}
	}
	else {
		nextPos.X = endPos.X;
	}
	if (startPos.Y < endPos.Y) {
		if (nextPos.Y >= endPos.Y) {
			nextPos.Y = endPos.Y;

		}
	}
	else if (startPos.Y > endPos.Y) {
		if (nextPos.Y <= endPos.Y) {
			nextPos.Y = endPos.Y;
		}
	}
	else {
		nextPos.Y = endPos.Y;
	}
	if (startPos.Z < endPos.Z) {
		if (nextPos.Z >= endPos.Z) {
			nextPos.Z = endPos.Z;
		}
	}
	else if (startPos.Z > endPos.Z) {
		if (nextPos.Z <= endPos.Z) {
			nextPos.Z = endPos.Z;
		}
	}
	else {
		nextPos.Z = endPos.Z;
	}
	return nextPos;
}
