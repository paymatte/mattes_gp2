// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "EnemyActorComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UEnemyActorComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UEnemyActorComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
public:
	UPROPERTY(EditAnywhere, Category = "Waypoints")
	TArray<FVector> Waypoints;
	UPROPERTY(EditAnywhere, Category = "Movement")
	float MoveSpeed = 100.f;
	int index = 0;
private:
	UFUNCTION(BlueprintCallable, Category = "Movement")
	static FVector MoveTowards( FVector& startPos, FVector& endPos, float maxDistance);
		
};
