// Fill out your copyright notice in the Description page of Project Settings.


#include "Milestone1/TankPawn.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "ProjectileActor.h"
#include "ShootingSceneComponent.h"

// Sets default values
ATankPawn::ATankPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	SetRootComponent(StaticMesh);
	TurretMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TurretMesh"));
	TurretMesh->SetupAttachment(GetRootComponent());

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	ProjectilePosition = CreateDefaultSubobject<UShootingSceneComponent>(TEXT("ProjectilePos"));

	SpringArmComponent->SetupAttachment(GetRootComponent());
	CameraComponent->SetupAttachment(SpringArmComponent);
	ProjectilePosition->SetupAttachment(TurretMesh);


	SpringArmComponent->bUsePawnControlRotation = true;
}

// Called when the game starts or when spawned
void ATankPawn::BeginPlay()
{
	Super::BeginPlay();
	//APlayerController* PlayerController = Cast<APlayerController>(Controller);
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller)) {
		if (ULocalPlayer* LocalPlayer = PlayerController->GetLocalPlayer()) {
			if (UEnhancedInputLocalPlayerSubsystem* EILPSubsystem =
				ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(LocalPlayer)) {
				if (EILPSubsystem) {
					EILPSubsystem->AddMappingContext(PawnContext, 0);
				}

			}
		}

	}
	
}

// Called every frame
void ATankPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATankPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent)) {
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ATankPawn::MoveCallback);
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &ATankPawn::LookCallback);
		EnhancedInputComponent->BindAction(ShootAction, ETriggerEvent::Triggered, this, &ATankPawn::ShootCallback);
	}

}

void ATankPawn::MoveCallback(const FInputActionValue& Value)
{
	FVector2D MoveInput = Value.Get<FVector2D>();
	UE_LOG(LogTemp, Display, TEXT("MOVE: %s"), *MoveInput.ToString());
	//type UELOg
	//UE_LOG(LogTemp, Display, TEXT(""));
	float DeltaTime = GetWorld()->GetDeltaSeconds();

	FVector MoveDirection = GetActorForwardVector() * MoveInput.Y * MoveSpeed * DeltaTime;
	AddActorWorldOffset(MoveDirection, true);

	FQuat RotationAmount = FQuat(GetActorUpVector(), MoveInput.X * FMath::DegreesToRadians(RotationSpeed) * DeltaTime);

	AddActorWorldRotation(RotationAmount, true);

	RotateTurret();

}

void ATankPawn::LookCallback(const FInputActionValue& Value)
{
	FVector2D LookInput = Value.Get<FVector2D>();
	//UE_LOG(LogTemp, Display, TEXT("look: %s"), *LookInput.ToString());

	AddControllerPitchInput(LookInput.Y);//look up 
	AddControllerYawInput(LookInput.X);//look right
	RotateTurret();
}

void ATankPawn::ShootCallback(const FInputActionValue& Value)
{
	UE_LOG(LogTemp, Display, TEXT("SHOOT"));

	AProjectileActor* Bullet = GetWorld()->SpawnActor<AProjectileActor>(ProjectileA, ProjectilePosition->GetComponentLocation(), ProjectilePosition->GetComponentRotation());
	Bullet->Launch();
}

void ATankPawn::RotateTurret()
{
	//Get th eforward directon of the w
	if (Controller) {
		FVector ForwardDirection = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
		ForwardDirection.Z = 0.f;//remove verical Component
		ForwardDirection.Normalize();
		//set world rotation to match forward direction
		TurretMesh->SetWorldRotation(ForwardDirection.Rotation());

	}


}

