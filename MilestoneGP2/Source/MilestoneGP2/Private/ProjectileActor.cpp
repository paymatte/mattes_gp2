// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileActor.h"
#include "GameFramework/ProjectileMovementComponent.h"

// Sets default values
AProjectileActor::AProjectileActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(FName("Projectile Movement"));
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	SetRootComponent(StaticMesh);
	//ProjectileMovement->SetupAttachment(GetRootComponent());

	//ProjectileMovement->bAutoActivate = false;

}

// Called when the game starts or when spawned
void AProjectileActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProjectileActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (isLaunch) {
		AddActorLocalOffset(FVector::ForwardVector * DeltaTime * Speed);
	}
}

void AProjectileActor::Launch()
{
	isLaunch = true;
	//ProjectileMovement->SetVelocityInLocalSpace(FVector::ForwardVector * Speed);
	//ProjectileMovement->Activate();
}

