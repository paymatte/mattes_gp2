// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProjectileActor.generated.h"
class UProjectileMovementComponent;

UCLASS()
class AProjectileActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectileActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Launch();

private:
	//UPROPERTY(EditAnywhere, Category = "Component")
	//UProjectileMovementComponent* ProjectileMovement;
	UPROPERTY(EditAnywhere, Category = "Component")
	float Speed = 50.f;
	bool isLaunch = false;
private:
	UPROPERTY(EditAnywhere, Category = "Components")
	TObjectPtr<UStaticMeshComponent> StaticMesh;
};
