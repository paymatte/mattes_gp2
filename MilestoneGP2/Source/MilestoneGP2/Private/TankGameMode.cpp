// Fill out your copyright notice in the Description page of Project Settings.


#include "TankGameMode.h"
#include "TankPlayerController.h"
#include "Milestone1/TankPawn.h"

ATankGameMode::ATankGameMode()
{
	PrimaryActorTick.bCanEverTick = true;

	DefaultPawnClass = ATankPawn::StaticClass();
	PlayerControllerClass = ATankPlayerController::StaticClass();
}

void ATankGameMode::BeginPlay()
{
	Super::BeginPlay();
}

void ATankGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
