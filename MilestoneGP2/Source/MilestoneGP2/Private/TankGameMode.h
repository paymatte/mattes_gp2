// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "TankGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ATankGameMode : public AGameMode
{
	GENERATED_BODY()
public:
	ATankGameMode();
protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

};
