// Fill out your copyright notice in the Description page of Project Settings.


#include "TankPlayerController.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"

void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();


	if (UEnhancedInputLocalPlayerSubsystem* EILPSubsystem =
		ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer())) {
		if (EILPSubsystem) {
			EILPSubsystem->AddMappingContext(ControllerContext, 0);
		}

	}
}

void ATankPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent)) {
		EnhancedInputComponent->BindAction(PauseAction, ETriggerEvent::Triggered, this, &ATankPlayerController::PauseCallback);
	}
}

void ATankPlayerController::PauseCallback(const FInputActionValue& Value)
{
	UE_LOG(LogTemp, Display, TEXT("PauseGAME"));

	if (IsPaused()) {
		SetPause(false);
	}
	else {
		Pause();
	}
}
