// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

class UInputMappingContext;
class UInputAction;
struct FInputActionValue;

/**
 * 
 */
UCLASS()
class ATankPlayerController : public APlayerController
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	virtual void SetupInputComponent() override;

private:
	UPROPERTY(EditAnywhere, Category = "InputTwo")
	TObjectPtr<UInputMappingContext> ControllerContext;

	UPROPERTY(EditAnywhere, Category = "InputTwo")
	TObjectPtr<UInputAction> PauseAction;


private:
	void PauseCallback(const FInputActionValue& Value);
	
};
